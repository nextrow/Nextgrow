function updatePricing() {

    const userSlider = document.getElementById('userSlider');
    const userCount = document.getElementById('userCount');
    const selectedUsers = parseInt(userSlider.value);
    userCount.textContent = selectedUsers;

    $('.card-deck .card').removeClass('highlighted');
    if (selectedUsers <= 10) {
        $('.card-deck .card:eq(0)').addClass('highlighted');
    } else if (selectedUsers <= 20) {
        $('.card-deck .card:eq(1)').addClass('highlighted');
    } else if (selectedUsers <= 30) {
        $('.card-deck .card:eq(2)').addClass('highlighted');
    }
}

const userSlider = document.getElementById('userSlider');
userSlider.addEventListener('input', updatePricing);
updatePricing();


// lazy loading
const resultsDiv = document.getElementById('results');

const loader = document.getElementById('loading');

let page = 1;

let loading = false;

function fetchData() {

    if (loading) return;

    loading = true;

    loader.style.display = 'block';

    fetch(`https://jsonplaceholder.typicode.com/posts?_page=${page}&_limit=10`)
        .then(response => response.json())
        .then(data => {

            loader.style.display = 'none';

            data.forEach(item => {

                const resultItem = document.createElement('div');

                resultItem.classList.add('result-item');

                resultItem.textContent = item.title;

                resultsDiv.appendChild(resultItem);

            });

            page++;

            loading = false;
        })
        .catch(error => {

            console.error('Error fetching data:', error);

            loading = false;
        });
}

function checkScroll() {

    const scrollTop = document.documentElement.scrollTop || document.body.scrollTop;

    const windowHeight = window.innerHeight;

    const documentHeight = document.documentElement.offsetHeight;

    if (scrollTop + windowHeight >= documentHeight - 100) {

        fetchData();

    }
}

window.addEventListener('scroll', checkScroll);

fetchData();